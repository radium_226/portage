*/*::booboo
*/*::eroen
*/*::miramir
*/*::seeds
*/*::steam
*/*::stuff
*/*::tlp
*/*::tox-overlay
*/*::jorgicio


=x11-terms/guake-0.5.0
#>=net-im/skype-4.3.0.37
>=sys-power/pm-utils-1.4.1-r6
# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/profiles/arch/amd64/no-emul-linux-x86/package.mask,v 1.2 2014/10/17 20:28:26 mgorny Exp $

# Michał Górny <mgorny@gentoo.org> (14 Sep 2014)
# on behalf of gx86-multilib project <multilib@gentoo.org>
# Mask emul-linux-x86 packages.
#app-emulation/emul-linux-x86-*
#app-emulation/emul-linux-x86-baselibs
app-emulation/emul-linux-x86-cpplibs
app-emulation/emul-linux-x86-db
app-emulation/emul-linux-x86-gstplugins
app-emulation/emul-linux-x86-gtklibs
app-emulation/emul-linux-x86-gtkmmlibs
app-emulation/emul-linux-x86-java
app-emulation/emul-linux-x86-jna
app-emulation/emul-linux-x86-medialibs
app-emulation/emul-linux-x86-motif
app-emulation/emul-linux-x86-opengl
#app-emulation/emul-linux-x86-qtlibs #needs for skype
app-emulation/emul-linux-x86-sdl
app-emulation/emul-linux-x86-soundlibs
app-emulation/emul-linux-x86-xlibs


# Michał Górny <mgorny@gentoo.org> (17 Sep 2014)
# on behalf of gx86-multilib project <multilib@gentoo.org>
# Old version of packages that do not fully support multilib deps. Those
# versions will be removed along with emul-linux-x86 packages.
<=app-emulation/wine-1.7.18

# Michał Górny <mgorny@gentoo.org> (17 Sep 2014)
# on behalf of gx86-multilib project <multilib@gentoo.org>
# Packages that do not support multilib deps at all. Those packages will
# need to be updated before emul-linux-x86 packages are removed.
app-emulation/crossover-bin


#this packages causes slot conflicts
=dev-libs/liborcus-0.7.0
=app-text/libetonyek-0.1.1
>=app-text/poppler-0.28.1
#=x11-base/xorg-server-1.16.1
>x11-base/xorg-server-1.15.49
=dev-libs/libixion-0.7.0
